import express from 'express';
import fs from 'fs';

const app = express();

app.get('/', (req, res) => {
    const foundCSV = fs.readFileSync('./document/user.csv')
    const stringCSV = Buffer.from(foundCSV).toString();
    const lines = stringCSV.split('\n');
    const users = []

    lines.forEach((line, index) => {
        if (index !== 0) {
            const newObject = {}
            const columns = lines[0].split(',') 
            columns.forEach((column, index) => {
                const columnsValue = line.split(',') 
                newObject[column] = columnsValue[index]
            })
            users.push(newObject)
        }
    });
    console.log(users)
    res.send('Well done!');
})

app.listen(3001, () => {
    console.log('The application is listening on port 3001!');
})