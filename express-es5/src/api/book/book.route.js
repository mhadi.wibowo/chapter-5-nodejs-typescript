const express = require('express')
const BookController = require('./book.controller')

const bookRouter = express.Router()
const bookController = new BookController()


bookRouter.route('/getAllBooks').get(bookController.getAllBooks)

module.exports = bookRouter
