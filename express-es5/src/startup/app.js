const express = require('express');
const app = express();
const fs = require('fs')
const multer = require('multer')
const axios = require('axios')
const bodyParser = require('body-parser')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})

require('dotenv').config()

const upload = multer({ storage: storage })
console.log('a1')
const response = require('../../helper/response')
const routes = require('./routes')

app.use(bodyParser.json())
app.use(routes)

app.get('/', (req, res) => {
    try {
        // const foundCSV = fs.readFileSync('./document/user123.csv') Jika mau test try catch
        const foundCSV = fs.readFileSync('./document/user.csv')
        const stringCSV = Buffer.from(foundCSV).toString();
        const lines = stringCSV.split('\n');
        const users = []

        lines.forEach((line, index) => {
            if (index !== 0) {
                const newObject = {}
                const columns = lines[0].split(',')
                columns.forEach((column, index) => {
                    const columnsValue = line.split(',')
                    newObject[column] = columnsValue[index]
                })
                users.push(newObject)
            }
        });
        response.success(res, 200, null, users, "Success Get User.")
    } catch (error) {
        console.log(error)
        response.errorInternal(res)
    }

});

app.post('/extractCSV', upload.single('file'), (req, res) => {
    try {
        const foundCSV = fs.readFileSync(`./uploads/${req.file.originalname}`)
        const stringCSV = Buffer.from(foundCSV).toString();
        const lines = stringCSV.split('\n');
        const users = []

        lines.forEach((line, index) => {
            if (index !== 0) {
                const newObject = {}
                const columns = lines[0].split(',')
                columns.forEach((column, index) => {
                    const columnsValue = line.split(',')
                    newObject[column] = columnsValue[index]
                })
                users.push(newObject)
            }
        });
        response.success(res, 200, null, users, "Success Extract User.")
    } catch (error) {
        console.log(error)
        response.errorInternal(res)
    }
});

app.get('/getBook/:book_id', (req, res) => {
    try {
        axios.get(`${process.env.BOOK_URL}/book/detail/${req.params.book_id}?key=${process.env.BOOK_KEY}`)
            .then(function (response) {
                return response.data
            })
            .catch(function (error) {
                console.log(error)
                response.errorInternal(res)
            })
            .then(function (data) {
                response.success(res, 200, data.meta, data.data, "Success Get a Book.")
            });
    } catch (error) {
        console.log(error)
        response.errorInternal(res)
    }
});

app.post('/createBook', (req, res) => {
    try {
        axios.post(`${process.env.BOOK_URL}/book/create?key=${process.env.BOOK_KEY}`, req.body)
            .then(function (response) {
                return response.data
            })
            .catch(function (error) {
                console.log(error)
                response.errorInternal(res)
            })
            .then(function (data) {
                response.success(res, 200, null, data.data, "Success Create a Book.")
            });
    } catch (error) {
        console.log(error)
        response.errorInternal(res)
    }
});

app.get('/deleteBook/:book_id', (req, res) => {
    try {
        axios.get(`${process.env.BOOK_URL}/book/delete/${req.params.book_id}?key=${process.env.BOOK_KEY}`)
            .then(function (response) {
                return response.data
            })
            .catch(function (error) {
                console.log(error)
                response.errorInternal(res)
            })
            .then(function (data) {
                response.success(res, 200, data.meta, data.data, "Success Delete a Book.")
            });
    } catch (error) {
        console.log(error)
        response.errorInternal(res)
    }
});

module.exports = app

// app.listen(3000, () => console.log('Gator app listening on port 3000!'));