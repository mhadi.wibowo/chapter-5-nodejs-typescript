const Response = {
    success: function(res, code, meta, data, message) {
        const response = {
            status: 'SUCCESS',
            message,
            code,
            meta,
            data
        }
        if (response.meta === null) {
            delete response.meta
        }
        res.status(code).send(response)
        return res
    },
    errorInternal: function(res) {
        const response = {
            status: 'FAILED',
            message: 'Internal Server Error',
            code: 500
        }
        res.status(500).send(response)
        return res
    },
}
module.exports = Response